#!/bin/bash

cd `dirname $0`

if [ $# -ne 1 ]; then
  echo "What's version?" 1>&2
  exit 1
fi

cd client
npm version $1 --no-git-tag-version

cd ../server
npm version $1 --no-git-tag-version

cd ..
git add .
git commit -m $1
