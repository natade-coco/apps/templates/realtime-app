# Realtime App

natadeCOCO realtime app template (vite-react + feathers)

## GitLabで本テンプレートを使用する方法

### 1. プロジェクト作成

1. `New project` => `Import project`
1. `Git repositiry URL` に https://gitlab.com/natade-coco/apps/templates/realtime-app.git を入力してプロジェクトを作成

### 2. クローン（サブモジュール込）

```
git clone --recursive git@gitlab/com:natade-coco/apps/xxxxx.git
```

### 3. サブモジュールの解除

```
git rm --cached app/client app/server
rm -rf app/client/.git app/server/.git
```

### 4. NPMインストール

```
cd app/client && npm install
cd ../server && npm install
```
